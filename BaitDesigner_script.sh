for i in `ls *masked.fasta`
    do
    sample=`echo $i | sed -e 's/_masked.fasta//g'`
    java -jar /home/ines/envpicard/share/picard-2.22.1-0/picard.jar BaitDesigner R="$i" T="$sample"_masked.list DESIGN_NAME="$sample"_probes BAIT_SIZE=180 RIGHT_PRIMER=CCCTATAGTGAGTCGTATTA LEFT_PRIMER=null  BAIT_OFFSET=180
    done

for i in `ls *masked.fasta`
    do
    sample=`echo $i | sed -e 's/_masked.fasta//g'`
    cat "$sample"_probes/"$sample"_probes.design.fasta >> rana_temporaria_mitochondrion_probes_T7.fasta
    done

sed '/^[^>]/s/[R|Y|W|S|M|K|H|B|V|D|N]//g' rana_temporaria_mitochondrion_probes_T7.fasta > rana_temporaria_mitochondrion_probes_T7_stringent.fasta