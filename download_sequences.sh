##download illumina read sequences from genome assembly UCB_Rtem_1.0 accession GCA_009802015.1
for i in `seq 1 9`; do wget https://sra-download.ncbi.nlm.nih.gov/traces/wgs01/wgs_aux/VI/AC/VIAC01/VIAC01."${i}".fsa_nt.gz; done
