#!/bin/bash

clean_dir=$1
mapping_dir=$2
reference=$3
bwa_tool=$4

samples=$(find $clean_dir -name '*.fastq.paired.fq' | sed 's/_R[1,2].*//' | sort -u)

for s in $samples
do
	# BWA
	pref_sample=$(basename $s)
	if [ $bwa_tool == 'mem' ]
	then
		bwa mem -t 4 $reference $s'_R1.fastq.paired.fq' $s'_R2.fastq.paired.fq' -R '@RG\tID:'$pref_sample'_on_ref\tSM:'$pref_sample'\tPL:ILLUMINA' 2> $mapping_dir'/logs/'$pref_sample'_on_ref.log' > $mapping_dir'/'$pref_sample'_on_ref.sam'
	elif [ $bwa_tool == 'aln' ]
	then
		bwa aln -t 4 $reference $s'_R1.fastq.paired.fq' -f $mapping_dir'/'$pref_sample'_aln_R1.sai' 2> $mapping_dir'/logs/'$pref_sample'_aln_R1.log' 
	        bwa aln -t 4 $reference $s'_R2.fastq.paired.fq' -f $mapping_dir'/'$pref_sample'_aln_R2.sai' 2> $mapping_dir'/logs/'$pref_sample'_aln_R2.log'
		bwa sampe $reference $mapping_dir'/'$pref_sample'_aln_R1.sai' $mapping_dir'/'$pref_sample'_aln_R2.sai' $s'_R1.fastq.paired.fq' $s'_R2.fastq.paired.fq' -f $mapping_dir'/'$pref_sample'_on_ref.sam' 2> $mapping_dir'/logs/'$pref_sample'_on_ref.log'
	fi	
	
	grep -v "^@" $mapping_dir'/'$pref_sample'_on_ref.sam' | awk '{print $3}' | sort | uniq -c > $mapping_dir'/'$pref_sample'_on_ref.sam_nb_reads'
	# BBMAP PILEUP
	pileup.sh in=$mapping_dir'/'$pref_sample'_on_ref.sam' out=$mapping_dir'/'$pref_sample'_on_ref.sam_covstats' &> $mapping_dir'/'$pref_sample'_on_ref.sam_stats'
	# SAMTOOLS
	samtools sort -O bam $mapping_dir'/'$pref_sample'_on_ref.sam' > $mapping_dir'/'$pref_sample'_on_ref_sorted.bam'
	samtools view -bF 4 $mapping_dir'/'$pref_sample'_on_ref_sorted.bam' > $mapping_dir'/'$pref_sample'_on_ref_sorted_filtered.bam'
done
