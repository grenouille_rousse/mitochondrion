#!/bin/bash

clean_dir=$1

samples=$(find $clean_dir -name '*.fastq' | sed 's/_R[1,2].*//' | sort -u)

for s in $samples
do
        fastq_pair $s'_R1.fastq' $s'_R2.fastq'
done
