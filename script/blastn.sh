#!/bin/bash

mapping_dir=$1
blast_dir=$2
blast_db=$3

for s in $mapping_dir/*.fasta
do
	echo $s
	pref=$(basename $s | sed 's/.fasta//')
	blastn -db $blast_db -query $s -out $blast_dir/$pref.out -outfmt 6
	ktImportBLAST $blast_dir/$pref'.out' -o $blast_dir/$pref'.html'
done
