mapping_dir=$1
variant_dir=$2
reference=$3

for s in $mapping_dir/*_filtered.bam
do
	prefix=$(basename $s .bam)
	dupout=$variant_dir'/'$prefix'_rmdup.bam'
	metricout=$variant_dir'/'$prefix'metrics.txt'
	[ ! -e "$dupout" ] && picard MarkDuplicates I=$s O=$dupout M=marks REMOVE_DUPLICATES=true 2> $variant_dir'/logs'/$prefix'_rmdup.log'
	[ ! -e "$metricout"] && picard ColllectWgsMetrics I=$dupout O=$metricout R=$reference 2> > $variant_dir'/logs'/$prefix'_metrics.log'
        [ ! -e "$" ]
done

freebayes -f $reference --ploidy 1 --pooled-continuous --min-alternate-qsum 30 --min-mapping-quality 20 --min-base-quality 30 $variant_dir/*_rmdup.bam > $variant_dir'/rana_mito.vcf' 2> $variant_dir'/logs/vcf.log'

vcftools --vcf $variant_dir'/rana_mito.vcf' --out $variant_dir'/rana_mito' --freq

beagle gt=$variant_dir'/rana_mito.vcf' out=$variant_dir'/rana_mito_phased' nthreads=10

bcftools stats $variant_dir'/rana_mito.vcf' > $variant_dir'/rana_mito_stats_vcf.txt'




