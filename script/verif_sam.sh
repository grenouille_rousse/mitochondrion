#!/bin/bash

mapping_dir=$1

sam_files=$(find $mappping_dir -name '*_on_ref.sam')

echo -e "|file|n_reads|n_mapped_reads|percent_mapped|\n|:----|:----|:----|:----|:----|"


for s in $sam_files
do
    reads=$(grep 'Reads:' $s'_stats' | awk {'print $NF'})
    mapped_reads=$(grep 'Mapped reads' $s'_stats' | awk {'print $NF'})
    percent_mapped=$(grep 'Percent mapped' $s'_stats' | awk {'print $NF'})
    echo "|"$s"|"$reads"|"$mapped_reads"|"$percent_mapped"|"
done
