#!/bin/bash

demultiplex_dir=$1
clean_dir=$2

for R1 in $demultiplex_dir/*R1.fastq.gz
do
	echo $R1
	output_name_1=$(basename $R1 | sed 's/.gz$//')
	cutadapt --cores 2 --cut -5 --quality-cutoff 15 --minimum-length 40 --adapter AGATCGGAAGAGC --output $clean_dir/$output_name_1 $R1
done

for R2 in $demultiplex_dir/*R2.fastq.gz
do
	echo $R2
	output_name_2=$(basename $R2 | sed 's/.gz$//')
	cutadapt --cores 2 --cut 5 --quality-cutoff 15 --minimum-length 40 --adapter AGATCGGAAGAGC --output $clean_dir/$output_name_2 $R2
done
