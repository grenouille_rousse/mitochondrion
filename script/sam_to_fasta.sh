#!/bin/bash

mapping_dir=$1

for s in $mapping_dir/*_on_ref.sam
do
        pref_sam=$(basename $s)
        samtools view -f 4 $s > $mapping_dir/$pref_sam'.unmapped.sam'
        samtools view -F 4 $s > $mapping_dir/$pref_sam'.mapped.sam'
        reformat.sh in=$mapping_dir/$pref_sam'.unmapped.sam' out=$mapping_dir/$pref_sam'.unmapped.fasta'
        reformat.sh in=$mapping_dir/$pref_sam'.mapped.sam' out=$mapping_dir/$pref_sam'.mapped.fasta'
done
